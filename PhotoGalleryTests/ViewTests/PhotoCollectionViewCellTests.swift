//
//  PhotoCollectionViewCellTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension PhotoCollectionViewCellTests {
    class FakeDataSource: NSObject, UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath)
            return cell
        }
    }
}

class PhotoCollectionViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var collectionView : UICollectionView!
    var cell : PhotoCollectionViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let photoGalleryVC = storyBoard.instantiateViewController(withIdentifier: "PhotoGalleryVC") as! PhotoGalleryVC
        UIApplication.shared.keyWindow?.rootViewController = photoGalleryVC
        _ = photoGalleryVC.view
        collectionView = photoGalleryVC.photosCollectionView
        collectionView.dataSource = fakeDataSource
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: IndexPath(row: 0, section: 0)) as! PhotoCollectionViewCell
    }
    
    func testPhotoCell_HasPhotoImageView() {
        // Assert
        XCTAssertNotNil(cell.photoImageView)
    }
    
}
