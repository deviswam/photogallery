//
//  PhotoGalleryVCTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension PhotoGalleryVCTests {
    class MockPhotoGalleryViewModel: PhotoGalleryViewModel {
        var getPublicPhotosCalled = false
        func getPublicPhotos(taggedWith tag: String?) {
            getPublicPhotosCalled = true
        }
        
        func numberOfPhotos() -> Int {
            return 0
        }
        
        func photo(at index: Int) -> PhotoViewModel? {
            return nil
        }
        
        func selectedPhoto(at index: Int) { }
        func sort(by option: PhotoGallerySortOption) {}
        func showAlert(with message: String) {}
        func showOptionsActionSheet() {}
    }
    
    class MockUICollectionView : UICollectionView {
        var reloadGotCalled = false
        override func reloadData() {
            reloadGotCalled = true
        }
    }
}

class PhotoGalleryVCTests: XCTestCase {
    var sut: PhotoGalleryVC!
    var mockViewModel: MockPhotoGalleryViewModel!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "PhotoGalleryVC") as! PhotoGalleryVC
        mockViewModel = MockPhotoGalleryViewModel()
        sut.viewModel = mockViewModel
    }
    
    func testPhotoGalleryVCHasAViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
    }
    
    func testPhotosCollectionView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.photosCollectionView)
    }
    
    func testPhotosCollectionView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.collectionViewDataSource = PhotosCollectionViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert((sut.photosCollectionView.dataSource as Any) is PhotosCollectionViewDataSource)
    }
    
    func testViewLoad_shouldAskViewModelForAllPublicPhotos() {
        //Act
        _ = sut.view
        
        //Assert
        XCTAssertTrue(mockViewModel.getPublicPhotosCalled,"view load should call view model for all public photos")
    }
    
    func testAfterHavingPhotos_ShouldReloadCollectionView() {
        //Arrange
        _ = sut.view
        let mockUICollectionView = MockUICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.photosCollectionView = mockUICollectionView
        
        //Act
        sut.photosLoaded(withResult: .success(()))
        
        //Assert
        XCTAssertTrue(mockUICollectionView.reloadGotCalled,"CollectionView Reload method should be called")
    }
}
