//
//  PhotoManagerTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension PhotoManagerTests {
    class MockPhotoAPIClient: PhotoAPIClient {
        var fetchPublicPhotosCalled = false
        var completionHandler: ((Result<[Photo]>) -> Void)?
        func fetchPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (Result<[Photo]>) -> Void) {
            fetchPublicPhotosCalled = true
            self.completionHandler = completionHandler
        }
        
        func photo(withImageUrl imageUrlStr: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
    }
}

class PhotoManagerTests: XCTestCase {
    var mockPhotoAPIClient: MockPhotoAPIClient!
    var sut: PhotoManagerImpl!
    
    override func setUp() {
        super.setUp()
        mockPhotoAPIClient = MockPhotoAPIClient()
        sut = PhotoManagerImpl(apiClient: mockPhotoAPIClient)
    }
    
    func testConformanceToPhotoManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is PhotoManager, "PhotoManagerImpl should conforms to PhotoManager protocol")
    }
    
    func testLoadPublicPhotos_asksPhotoAPIClientToFetchPublicPhotos() {
        // Act
        sut.loadPublicPhotos(taggedWith: nil) { result in
            
        }
        
        // Assert
        XCTAssert(mockPhotoAPIClient.fetchPublicPhotosCalled)
    }
    
    func testLoadPublicPhotos_shouldReceivePhotosWithNilError_whenAPIClientFoundPhotos() {
        // Arrange
        var receivedPhotos : [Photo]?
        var receivedError : Error?
        let expectedPhotos = [PhotoImpl()]
        
        // Act
        sut.loadPublicPhotos(taggedWith: nil) { result in
            switch result {
            case .success(let photos):
                receivedPhotos = photos
            case .failure(let error):
                receivedError = error
            }
        }

        mockPhotoAPIClient.completionHandler?(.success(expectedPhotos))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedPhotos)
    }
    
    func testLoadPublicPhotos_shouldReceivePhotosFoundError_whenAPIClientHasDecodingError() {
        // Arrange
        var receivedPhotos : [Photo]?
        var receivedError : Error?
        
        // Act
        sut.loadPublicPhotos(taggedWith: nil) { result in
            switch result {
            case .success(let photos):
                receivedPhotos = photos
            case .failure(let error):
                receivedError = error
            }
        }
        mockPhotoAPIClient.completionHandler?(.failure(PhotoAPIClientError.dataDecodingError))
        
        //Assert
        XCTAssertEqual(receivedError as! PhotoManagerError, PhotoManagerError.noPhotosFoundError, "Should receive noPhotosFoundError when underlying api has dataDecodingError")
        XCTAssertNil(receivedPhotos, "No photos are returned when error occured")
    }
    
    func testLoadPublicPhotos_shouldReceiveConnectionError_whenAPIClientHasHTTPError() {
        // Arrange
        var receivedPhotos : [Photo]?
        var receivedError : Error?
        
        // Act
        sut.loadPublicPhotos(taggedWith: nil) { result in
            switch result {
            case .success(let photos):
                receivedPhotos = photos
            case .failure(let error):
                receivedError = error
            }
        }
        mockPhotoAPIClient.completionHandler?(.failure(PhotoAPIClientError.httpError))
        
        //Assert
        XCTAssertEqual(receivedError as! PhotoManagerError, PhotoManagerError.connectionError, "Should receive connectionError when underlying api has httpError")
        XCTAssertNil(receivedPhotos, "No photos are returned when error occured")
    }
}
