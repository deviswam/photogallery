//
//  PhotoAPIClientTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension FlickrAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class FlickrAPIClientTests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: FlickrAPIClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = FlickrAPIClient(urlSession: mockURLSession)
    }
    
    func testConformanceToPhotoAPIClient() {
        XCTAssertTrue((sut as AnyObject) is PhotoAPIClient)
    }
    
    func testFetchPublicPhotos_shouldAskURLSessionForFetchingPublicPhotosData() {
        // Act
        sut.fetchPublicPhotos(taggedWith: nil) { result in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testFetchPublicPhotos_withValidJSONOfAPhoto_shouldReturnAPhotoAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let photosAPIResponseStr = """
        {
            "items": [{
                    "title": "Motion Detection",
                    "media": {
                        "m": "https://farm5.staticflickr.com/4696/25061029377_745fdccd35_m.jpg"
                    },
                    "published": "2018-01-27T20:23:07Z",
                    "author": "nobody@flickr.com"
            }]
        }
        """
        
        let responseData = photosAPIResponseStr.data(using: .utf8)
        var receivedPhoto : Photo?
        var receivedError : PhotoAPIClientError?
        
        //Act
        sut.fetchPublicPhotos(taggedWith: nil) { (result: Result<[Photo]>) in
            switch result {
            case .success(let photos):
                receivedPhoto = photos.first
            case .failure(let error):
                receivedError = error as? PhotoAPIClientError
            }
            asynExpectation.fulfill()
        }

        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedPhoto?.title, "Motion Detection")
            XCTAssertEqual(receivedPhoto?.imageUrl, "https://farm5.staticflickr.com/4696/25061029377_745fdccd35_m.jpg")
            XCTAssertEqual(receivedPhoto?.author, "nobody@flickr.com")
            XCTAssertNotNil(receivedPhoto?.publishedDate)
            XCTAssertNil(receivedError)
        }
    }
    
    func testFetchPublicPhotos_withValidJSONOfTwoPhoto_shouldReturnTwoPhotosAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let photosAPIResponseStr = """
        {
            "items": [{
                    "title": "Motion Detection",
                    "media": {
                        "m": "https://farm5.staticflickr.com/4696/25061029377_745fdccd35_m.jpg"
                    },
                    "published": "2018-01-27T20:23:07Z",
                    "author": "nobody@flickr.com"
            },{
                    "title": "iPhone",
                    "media": {
                        "m": "https://farm5.staticflickr.com/96/2529377_7455_m.jpg"
                    },
                    "published": "2017-01-23T20:23:07Z",
                    "author": "hello@flickr.com"
            }]
        }
        """
        
        let responseData = photosAPIResponseStr.data(using: .utf8)
        var receivedPhotos : [Photo]?
        var receivedError : PhotoAPIClientError?
        
        //Act
        sut.fetchPublicPhotos(taggedWith: nil) { (result: Result<[Photo]>) in
            switch result {
            case .success(let photos):
                receivedPhotos = photos
            case .failure(let error):
                receivedError = error as? PhotoAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedPhotos?[0].title, "Motion Detection")
            XCTAssertEqual(receivedPhotos?[0].imageUrl, "https://farm5.staticflickr.com/4696/25061029377_745fdccd35_m.jpg")
            XCTAssertEqual(receivedPhotos?[0].author, "nobody@flickr.com")
            XCTAssertNotNil(receivedPhotos?[0].publishedDate)
            
            XCTAssertEqual(receivedPhotos?[1].title, "iPhone")
            XCTAssertEqual(receivedPhotos?[1].imageUrl, "https://farm5.staticflickr.com/96/2529377_7455_m.jpg")
            XCTAssertEqual(receivedPhotos?[1].author, "hello@flickr.com")
            XCTAssertNotNil(receivedPhotos?[1].publishedDate)
            
            XCTAssertNil(receivedError)
        }
    }
    
    func testFetchPublicPhotos_withInValidJSONOfAPhoto_shouldReturnDataDecodingError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let photosAPIResponseStr = """
        {
            "itemss": [{
                    "title": "Motion Detection",
                    "media": {
                        "m": "https://farm5.staticflickr.com/4696/25061029377_745fdccd35_m.jpg"
                    },
                    "published": "2018-01-27T20:23:07Z",
                    "author": "nobody@flickr.com"
            }]
        }
        """
        
        let responseData = photosAPIResponseStr.data(using: .utf8)
        var receivedPhotos : [Photo]?
        var receivedError : PhotoAPIClientError?
        
        //Act
        sut.fetchPublicPhotos(taggedWith: nil) { (result: Result<[Photo]>) in
            switch result {
            case .success(let photos):
                receivedPhotos = photos
            case .failure(let error):
                receivedError = error as? PhotoAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == PhotoAPIClientError.dataDecodingError)
            XCTAssertNil(receivedPhotos)
        }
    }
}
