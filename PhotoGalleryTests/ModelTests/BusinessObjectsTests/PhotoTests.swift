//
//  PhotoTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

class PhotoTests: XCTestCase {
    
    func testConformanceToPhotoProtocol() {
        // Arrange
        let sut = PhotoImpl()

        // Assert
        XCTAssert((sut as Any) is Photo)
    }
    
    func testConformanceToDecodableProtocol() {
        // Arrange
        let sut = PhotoImpl()
        // Assert
        XCTAssert((sut as Any) is Decodable)
    }
    
    func testInit_photoShouldHaveATitle() {
        // Arrange
        let photoTitle = "iPhonePhoto"
        let sut = PhotoImpl(title: photoTitle)
        // Assert
        XCTAssertEqual(sut.title, photoTitle)
    }
    
    func testInit_photoShouldHaveAPublishedDate() {
        // Arrange
        let date = Date()
        let sut = PhotoImpl(publishedDate: date)
        // Assert
        XCTAssertEqual(sut.publishedDate, date)
    }
    
    func testInit_withInvalidPhotoJSONData_shouldFailPhotoDecoding() {
        //Arrange
        let sut = try? JSONDecoder().decode(PhotoImpl.self, from: Data())
        
        //Assert
        XCTAssertNil(sut, "Photo should be created only with valid json data")
    }
    
    func testInit_withPhotoTitleInJSONData_shouldSetPhotoTitle() {
        // Arrange
        let photoTitle = "iPhonePhoto"
        let photoData = """
        {
                "title" : "\(photoTitle)"
        }
        """

        let photo = try? JSONDecoder().decode(PhotoImpl.self, from: photoData.data(using: .utf8)!)

        // Assert
        XCTAssertEqual(photo?.title, photoTitle)
    }
    
    func testInit_withMediaPathInJSONData_shouldSetPhotoImageUrl() {
        // Arrange
        let imageUrlStr = "www.images.com/123.jpg"
        let photoData = """
        {
        \"media\" :
            {
                \"m\" : \"\(imageUrlStr)\"
            }
        }
        """

        let photo = try? JSONDecoder().decode(PhotoImpl.self, from: photoData.data(using: .utf8)!)

        // Assert
        XCTAssertEqual(photo?.imageUrl, imageUrlStr)
    }
    
    func testInit_withPhotoAuthorInJSONData_shouldSetPhotoAuthor() {
        // Arrange
        let photoAuthor = "nobody@gmail.com"
        let photoData = """
        {
        "author" : "\(photoAuthor)"
        }
        """
        
        let photo = try? JSONDecoder().decode(PhotoImpl.self, from: photoData.data(using: .utf8)!)
        
        // Assert
        XCTAssertEqual(photo?.author, photoAuthor)
    }
    
    func testInit_withPhotoPublishedDateInJSONData_shouldSetPublishedDate() {
        // Arrange
        let publishedDateStr = "2018-01-27T17:13:50Z"
        let photoData = """
        {
        "published" : "\(publishedDateStr)"
        }
        """
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let photo = try? decoder.decode(PhotoImpl.self, from: photoData.data(using: .utf8)!)
        
        // Assert
        XCTAssertNotNil(photo?.publishedDate)
    }
}
