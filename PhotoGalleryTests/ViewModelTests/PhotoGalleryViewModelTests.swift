//
//  PhotoGalleryViewModelTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension PhotoGalleryViewModelTests {
    class MockPhotoManager: PhotoManager {
        var loadPublicPhotosCalled = false
        var completionHandler: ((Result<[Photo]>) -> Void)?
        func loadPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (Result<[Photo]>) -> Void) {
            loadPublicPhotosCalled = true
            self.completionHandler = completionHandler
        }
    }
    
    class MockPhotoGalleryViewModelViewDelegate: PhotoGalleryViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var photoGalleryViewModelResult: Result<Void>?
        
        func photosLoaded(withResult result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockPhotoGalleryViewModelViewDelegate is not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.photoGalleryViewModelResult = result
            asynExpectation.fulfill()
        }
        
        func photosLoadStarted() {
        }
    }
    
    class MockPhotoGalleryViewModelCoordinatorDelegate: PhotoGalleryViewModelCoordinatorDelegate {
        func showAlert(with message: String) { }
        func showOptionsActionSheet() {}
        func didSelect(photo: Photo) {}
    }
}

class PhotoGalleryViewModelTests: XCTestCase {
    
    var sut: PhotoGalleryViewModelImpl!
    var mockPhotoManager: MockPhotoManager!
    var mockViewDelegate: MockPhotoGalleryViewModelViewDelegate!
    var mockCoordinatorDelegate: MockPhotoGalleryViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        mockPhotoManager = MockPhotoManager()
        mockViewDelegate = MockPhotoGalleryViewModelViewDelegate()
        mockCoordinatorDelegate = MockPhotoGalleryViewModelCoordinatorDelegate()
        sut = PhotoGalleryViewModelImpl(photoManager: mockPhotoManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testConformanceToPhotoGalleryViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is PhotoGalleryViewModel, "PhotoGalleryViewModelImpl should conforms to PhotoGalleryViewModel protocol")
    }
    
    func testGetPublicPhotos_shouldAskPhotoManagerForListOfPhotos() {
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        // Assert
        XCTAssert(mockPhotoManager.loadPublicPhotosCalled)
    }
    
    func testGetPublicPhotos_whenPhotosFound_raisesPhotosLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.success([PhotoImpl()]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.photoGalleryViewModelResult! {
            case .success():
                XCTAssert(true)
            default:
                XCTFail("View delegate should only receive success result when photos found")
            }
        }
    }
    
    func testGetPublicPhotos_whenNoPhotosFound_raisesPhotosLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.failure(PhotoManagerError.noPhotosFoundError))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.photoGalleryViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO photos found")
            case .failure(let error):
                XCTAssertEqual(error as! PhotoManagerError, PhotoManagerError.noPhotosFoundError)
            }
        }
    }
    
    func testNumberOfPhotos_whenNoPhotosFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.failure(PhotoManagerError.noPhotosFoundError))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfPhotos(), 0)
        }
    }
    
    func testNumberOfPhotos_whenOnePhotoFound_returnsOne() {
        // Arrange
        let photos = [PhotoImpl()]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.success(photos))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfPhotos(), 1)
        }
    }
    
    func testNumberOfPhotos_whenThreePhotoFound_returnsThree() {
        // Arrange
        let photos = [PhotoImpl(), PhotoImpl(), PhotoImpl()]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.success(photos))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfPhotos(), 3)
        }
    }
    
    func testPhotoAtIndex_returnsTheCorrectPhotoViewModelObject() {
        // Arrange
        let photos = [PhotoImpl(title: "iPhonePhoto"), PhotoImpl(title: "iPodPhoto")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.success(photos))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let photo1 = self.sut.photo(at: 0)
            let photo2 = self.sut.photo(at: 1)
            XCTAssertEqual(photo1?.title, "iPhonePhoto")
            XCTAssertEqual(photo2?.title, "iPodPhoto")
        }
    }
    
    func testPhotoAtIndex_whenPhotoNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let photos = [PhotoImpl(title: "iPhonePhoto"), PhotoImpl(title: "iPodPhoto")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getPublicPhotos(taggedWith: nil)
        mockPhotoManager.completionHandler?(Result.success(photos))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let photo = self.sut.photo(at: 2)
            XCTAssertNil(photo)
        }
    }
    
    
}
