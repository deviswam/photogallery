//
//  PhotosCollectionViewDataSourceTests.swift
//  PhotoGalleryTests
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import PhotoGallery

extension PhotosCollectionViewDataSourceTests {
    class MockPhotoGalleryViewModel: PhotoGalleryViewModel {
        var photos: [Photo]?
        func numberOfPhotos() -> Int {
            return photos!.count
        }
        
        func photo(at index: Int) -> PhotoViewModel? {
            return PhotoViewModelImpl(photo: photos![index])
        }
        
        func getPublicPhotos(taggedWith tag: String?) {}
        func selectedPhoto(at index: Int) {}
        func sort(by option: PhotoGallerySortOption) {}
        func showAlert(with message: String) {}
        func showOptionsActionSheet() {}
    }
    
    class MockCollectionView: UICollectionView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        }
    }
}

class PhotosCollectionViewDataSourceTests: XCTestCase {
    var sut: PhotosCollectionViewDataSource!
    var mockCollectionView: MockCollectionView!
    var mockViewModel: MockPhotoGalleryViewModel!
    
    override func setUp() {
        super.setUp()
        sut = PhotosCollectionViewDataSource()
        mockViewModel = MockPhotoGalleryViewModel()
        mockCollectionView = MockCollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.viewModel = mockViewModel
        mockCollectionView.dataSource = sut
    }
    
    func testDataSourceHasPhotoGalleryViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is PhotoGalleryViewModel)
    }
    
    func testNumberOfSections_IsOne() {
        //Arrange
        let noOfSections = sut.numberOfSections(in: mockCollectionView)
        //Assert
        XCTAssertEqual(noOfSections, 1,"CollectionView has only one section")
    }
    
    func testNumberOfItemsInTheSection_WithTwoPhotos_ShouldReturnTwo() {
        //Arrange
        let photos = [PhotoImpl(), PhotoImpl()]
        mockViewModel.photos = photos
        
        //Act
        let noOfItemsInSectionZero = sut.collectionView(mockCollectionView, numberOfItemsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForItem_ReturnsPhotoCollectionViewCell() {
        //Arrange
        let photos = [PhotoImpl(), PhotoImpl()]
        mockViewModel.photos = photos
        
        mockCollectionView.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: "PhotoCell")
        
        //Act
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is PhotoCollectionViewCell,"Returned cell is of PhotoCollectionViewCell type")
    }
    
    func testCellForItem_DequeuesCell() {
        //Arrange
        let photos = [PhotoImpl(), PhotoImpl()]
        mockViewModel.photos = photos
        
        mockCollectionView.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: "PhotoCell")
        
        //Act
        _ = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockCollectionView.cellGotDequeued,"CellForItem should be calling DequeueCell method")
    }
}
