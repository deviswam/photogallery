//
//  PhotoPreviewCoordinator.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotoPreviewCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var selectedPhoto: Photo
    private var photoPreviewVC: PhotoPreviewVC?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController, selectedPhoto: Photo) {
        self.navigationController = navController
        self.selectedPhoto = selectedPhoto
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let photoPreviewVC = storyboard.instantiateViewController(withIdentifier: "PhotoPreviewVC") as? PhotoPreviewVC {
            self.photoPreviewVC = photoPreviewVC
            let photoPreviewViewModel = PhotoPreviewViewModelImpl(selectedPhoto: selectedPhoto, coordinatorDelegate: self)
            
            photoPreviewVC.viewModel = photoPreviewViewModel
            self.navigationController.topViewController?.present(photoPreviewVC, animated: true, completion: nil)
        }
    }
}

extension PhotoPreviewCoordinator: PhotoPreviewViewModelCoordinatorDelegate {
    func dismissView() {
        self.navigationController.dismiss(animated: true, completion: nil)
    }
    
    func showActivityView(with items: [Any]) {
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.photoPreviewVC?.present(activityViewController, animated: true, completion: nil)
    }
}

