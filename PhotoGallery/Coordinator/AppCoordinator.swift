//
//  AppCoordinator.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var window: UIWindow
    private var photoGalleryCoordinator : PhotoGalleryCoordinator?
    
    // MARK:- INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showPhotoGallery(rootNavController: rootNavController)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func showPhotoGallery(rootNavController: UINavigationController) {
        photoGalleryCoordinator = PhotoGalleryCoordinator(navController: rootNavController)
        photoGalleryCoordinator?.start()
    }
}
