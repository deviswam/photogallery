//
//  PhotoGalleryCoordinator.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotoGalleryCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var photoPreviewCoordinator: PhotoPreviewCoordinator?
    private var photoGalleryViewModel: PhotoGalleryViewModel?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let photoGalleryVC = navigationController.topViewController as? PhotoGalleryVC {
            let apiClient = SharedComponentsDir.flickrAPIClient
            let photoManager = PhotoManagerImpl(apiClient: apiClient)
            let photosCollectionViewDataSource = PhotosCollectionViewDataSource()
            let photosCollectionViewDelegate = PhotosCollectionViewDelegate()
            let photosSearchBarDelegate = PhotosSearchBarDelegate()
            photoGalleryViewModel = PhotoGalleryViewModelImpl(photoManager: photoManager, viewDelegate: photoGalleryVC, coordinatorDelegate: self)
            
            photoGalleryVC.viewModel = photoGalleryViewModel
            photoGalleryVC.collectionViewDataSource = photosCollectionViewDataSource
            photoGalleryVC.collectionViewDelegate = photosCollectionViewDelegate
            photoGalleryVC.photosSearchBarDelegate = photosSearchBarDelegate
            photosCollectionViewDataSource.viewModel = photoGalleryViewModel
            photosCollectionViewDelegate.viewModel = photoGalleryViewModel
            photosSearchBarDelegate.viewModel = photoGalleryViewModel
        }
    }
}

extension PhotoGalleryCoordinator: PhotoGalleryViewModelCoordinatorDelegate {
    func didSelect(photo: Photo) {
        photoPreviewCoordinator = PhotoPreviewCoordinator(navController: self.navigationController, selectedPhoto: photo)
        photoPreviewCoordinator?.start()
    }
    
    func showAlert(with message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        navigationController.topViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showOptionsActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Sort by published date", style: .default, handler: { [weak self] _ in
            self?.photoGalleryViewModel?.sort(by: .publishedDate) // sort by putting most recent published photo on top.
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        navigationController.topViewController?.present(actionSheet, animated: true, completion: nil)
    }
}
