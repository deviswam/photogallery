//
//  PhotoCollectionViewCell.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    
    func setImage(image: UIImage) {
        self.photoImageView.image = image
    }
    
    override func prepareForReuse() {
        self.photoImageView.image = UIImage(named: "placeholder")
    }
}
