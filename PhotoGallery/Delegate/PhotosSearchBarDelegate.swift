//
//  PhotosSearchBarDelegate.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotosSearchBarDelegate: NSObject, UISearchBarDelegate {
    var viewModel: PhotoGalleryViewModel?
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let viewModel = viewModel, let searchText = searchBar.text {
            viewModel.getPublicPhotos(taggedWith: searchText)
            searchBar.resignFirstResponder()
        }
    }
}
