//
//  SharedComponentsDir.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let flickrAPIClient: PhotoAPIClient = {
        let flickrAPIClient = FlickrAPIClient()
        return flickrAPIClient
    }()
}
