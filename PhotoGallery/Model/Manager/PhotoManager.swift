//
//  PhotoManager.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum PhotoManagerError : Error {
    case noPhotosFoundError
    case connectionError
    
    var localizedDescription: String {
        switch self {
        case .noPhotosFoundError:
            return "No Photos Found"
        case .connectionError:
            return "Connection error"
        }
    }
}

protocol PhotoManager {
    // get public photos
    func loadPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (_ result: Result<[Photo]>) -> Void)
}

class PhotoManagerImpl: PhotoManager {
    // MARK: PRIVATE VARIABLES
    private let apiClient: PhotoAPIClient
    
    // MARK: INITIALIZER
    init(apiClient: PhotoAPIClient) {
        self.apiClient = apiClient
    }
    
    // MARK: PUBLIC METHODS
    func loadPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (_ result: Result<[Photo]>) -> Void) {
        apiClient.fetchPublicPhotos(taggedWith: tag) { [unowned self] (result: Result<[Photo]>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.photoManagerError(from: error as! PhotoAPIClientError))
            }
            completionHandler(mResult)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func photoManagerError(from apiError: PhotoAPIClientError) -> PhotoManagerError {
        switch apiError {
        case .dataDecodingError, .invalidDataError:
            return PhotoManagerError.noPhotosFoundError
        case .httpError:
            return PhotoManagerError.connectionError
        }
    }
}
