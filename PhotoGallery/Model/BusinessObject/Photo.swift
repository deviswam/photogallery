//
//  Photo.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol Photo: Decodable {
    var title: String? { get }
    var imageUrl: String? { get }
    var publishedDate: Date? { get }
    var author: String? { get }
}

struct PhotoImpl: Photo {
    var title: String?
    var author: String?
    var imageUrl: String?
    var publishedDate: Date?
    
    init(title: String? = nil, imageUrl: String? = nil, publishedDate: Date? = nil, author: String? = nil) {
        self.title = title
        self.author = author
        self.imageUrl = imageUrl
        self.publishedDate = publishedDate
    }
    
    enum CodingKeys: String, CodingKey {
        case title
        case author
        case publishedDate = "published"
        case media = "media"
        case imageUrl = "m"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        author = try values.decodeIfPresent(String.self, forKey: .author)
        publishedDate = try values.decodeIfPresent(Date.self, forKey: .publishedDate)
        
        let mediaValues = try? values.nestedContainer(keyedBy: CodingKeys.self, forKey: .media)
        imageUrl = try mediaValues?.decodeIfPresent(String.self, forKey: .imageUrl)
    }
}
