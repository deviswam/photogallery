//
//  PhotoAPIClient.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

enum PhotoAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataDecodingError
}

protocol PhotoAPIClient {
    func fetchPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (_ result: Result<[Photo]>) -> Void)
    func photo(withImageUrl imageUrlStr: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class FlickrAPIClient: PhotoAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "https://api.flickr.com/services/feeds/"
    private let commonRequestParams = ["format": "json", "nojsoncallback": "1"]
    private let urlSession: URLSession!
    
    // MARK: PRIVATE DATA STRUCTURE
    private struct PublicPhotosFeed: Decodable {
        let items: [PhotoImpl]
    }
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func photo(withImageUrl imageUrlStr: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let url = URL(string: imageUrlStr) else {
            return
        }
        //print("WAM PHOTO URL:\(url.absoluteString)")
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            var result: Result<UIImage>!
            if error != nil {
                result = .failure(PhotoAPIClientError.httpError)
            } else if let data = data {
                let image = UIImage(data: data)
                if image == nil {
                    result = .failure(PhotoAPIClientError.invalidDataError)
                } else {
                    result = .success(image!)
                }
            }
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    func fetchPublicPhotos(taggedWith tag: String?, completionHandler: @escaping (_ result: Result<[Photo]>) -> Void) {
        var requestParams: [String: String] = [:]
        if let tag = tag {
            requestParams = ["tags": tag]
        }
        
        guard let urlRequest = self.createURLRequest(withParams: requestParams, forMethod: "photos_public.gne") else {
            return
        }
        
        self.getPhotos(with: urlRequest) { (result: Result<[Photo]>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func getPhotos(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[Photo]>) -> Void) {
        
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<[Photo]>!
            
            if error != nil {
                result = .failure(PhotoAPIClientError.httpError)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let publicPhotosFeed = try decoder.decode(PublicPhotosFeed.self, from: data)
                    result = .success(publicPhotosFeed.items)
                } catch {
                    //print("WAM: decoding actual error:", error)
                    result = .failure(PhotoAPIClientError.dataDecodingError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func createURLRequest(withParams parameters:[String: String], forMethod method: String) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: BASE_URL + method)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        // append common params
        commonRequestParams.forEach { queryItems.append(URLQueryItem(name: $0, value: $1))}
        
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
}
