//
//  PhotoGalleryVC.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 26/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotoGalleryVC: UIViewController {
    //MARK: INSTANCE VARIABLES
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var photosSearchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: PhotoGalleryViewModel?
    var collectionViewDataSource: UICollectionViewDataSource?
    var collectionViewDelegate: UICollectionViewDelegate?
    var photosSearchBarDelegate: UISearchBarDelegate?
    
    //MARK: PUBLIC METHODS
    @IBAction func optionsButtonPressed(_ sender: Any) {
        viewModel?.showOptionsActionSheet()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photosCollectionView.dataSource = collectionViewDataSource
        photosCollectionView.delegate = collectionViewDelegate
        photosSearchBar.delegate = photosSearchBarDelegate
        
        // register photo cell
        registerPhotoCell()
        
        // get all public photos
        viewModel?.getPublicPhotos(taggedWith: nil)
    }
    
    //MARK: PRIVATE METHODS
    private func registerPhotoCell() {
        let nib = UINib(nibName: "PhotoCollectionViewCell", bundle: Bundle.main)
        photosCollectionView.register(nib, forCellWithReuseIdentifier: "PhotoCell")
    }
}

extension PhotoGalleryVC: PhotoGalleryViewModelViewDelegate {
    func photosLoadStarted() {
        self.activityIndicator.startAnimating()
    }
    
    func photosLoaded(withResult result: Result<Void>) {
        switch result {
        case .success:
            self.photosCollectionView.reloadData()
        case .failure:
            viewModel?.showAlert(with: "Unable to fetch photos")
        }
        self.activityIndicator.stopAnimating()
    }
}
