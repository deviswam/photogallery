//
//  PhotoPreviewVC.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotoPreviewVC: UIViewController {
    //MARK: INSTANCE VARIABLES
    var viewModel: PhotoPreviewViewModel?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publishedLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    //MARK: PUBLIC METHODS
    @IBAction func crossButtonPressed(_ sender: Any) {
        viewModel?.dismissView()
    }
    
    @IBAction func actionButtonPressed(_ sender: Any) {
        guard let title = titleLabel.text,
            let photo = photoImageView.image else {
                return
        }
        viewModel?.showActivityView(with: [title, photo])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPhotoDetails()
    }
    
    func setPhotoDetails() {
        titleLabel.text = viewModel?.title
        publishedLabel.text = viewModel?.publishedOn
        authorLabel.text = viewModel?.author
        
        viewModel?.photoImage(completion: { [weak self] image in
            self?.photoImageView.image = image
        })
    }
}
