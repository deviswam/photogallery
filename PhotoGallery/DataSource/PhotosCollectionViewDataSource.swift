//
//  PhotosCollectionViewDataSource.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class PhotosCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var viewModel: PhotoGalleryViewModel?
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfPhotos()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath)
        if let photoCell = cell as? PhotoCollectionViewCell, let photoVM = viewModel?.photo(at: indexPath.item) {
            loadPhoto(with: photoVM.photo.imageUrl, onCell: photoCell)
        }
        return cell
    }
    
    private func loadPhoto(with photoPath: String?, onCell cell:PhotoCollectionViewCell) {
        guard let photoPath = photoPath else { return }
        ImageStore.getImage(withImagePath: photoPath) { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setImage(image: image)
            }
        }
    }
}
