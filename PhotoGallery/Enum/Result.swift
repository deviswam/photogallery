//
//  Result.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
