//
//  PhotoGalleryViewModel.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum PhotoGallerySortOption {
    case publishedDate // sort by putting most recent published photo on top.
}

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol PhotoGalleryViewModelCoordinatorDelegate: class {
    func didSelect(photo: Photo)
    func showAlert(with message: String)
    func showOptionsActionSheet()
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol PhotoGalleryViewModelViewDelegate: class {
    func photosLoadStarted()
    func photosLoaded(withResult result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol PhotoGalleryViewModel {
    func getPublicPhotos(taggedWith tag: String?)
    func sort(by option: PhotoGallerySortOption) // sort by putting most recent published photo on top.
    
    func numberOfPhotos() -> Int
    func photo(at index: Int) -> PhotoViewModel?
    func selectedPhoto(at index: Int)
    
    func showAlert(with message: String)
    func showOptionsActionSheet()
}

class PhotoGalleryViewModelImpl: PhotoGalleryViewModel {
    // MARK: PRIVATE VARIABLES
    private let photoManager: PhotoManager
    private weak var viewDelegate: PhotoGalleryViewModelViewDelegate!
    private weak var coordinatorDelegate: PhotoGalleryViewModelCoordinatorDelegate!
    
    private var photos: [Photo]?
    
    // MARK: INITIALIZER
    init(photoManager: PhotoManager, viewDelegate: PhotoGalleryViewModelViewDelegate, coordinatorDelegate: PhotoGalleryViewModelCoordinatorDelegate) {
        self.photoManager = photoManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getPublicPhotos(taggedWith tag: String?) {
        var vmResult: Result<Void>!
        photoManager.loadPublicPhotos(taggedWith: tag) { [weak self] result in
            switch result {
            case .success(let photos):
                self?.photos = photos
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.photosLoaded(withResult: vmResult)
        }
        self.viewDelegate.photosLoadStarted()
    }
    
    func numberOfPhotos() -> Int {
        guard let photos = self.photos else {
            return 0
        }
        return photos.count
    }
    
    func photo(at index: Int) -> PhotoViewModel? {
        if let photos = self.photos, index < photos.count {
            return PhotoViewModelImpl(photo: photos[index])
        }
        return nil
    }
    
    func selectedPhoto(at index: Int) {
        guard let photos = self.photos, index < photos.count else {
            return
        }
        self.coordinatorDelegate.didSelect(photo: photos[index])
    }
    
    func sort(by option: PhotoGallerySortOption) {
        guard let photos = self.photos else {
            return
        }
        
        self.photos = photos.sorted(by: { (photo1, photo2) -> Bool in
            if option == .publishedDate {
                guard let p1PubDate = photo1.publishedDate,
                    let p2PubDate = photo2.publishedDate else {
                        return false
                }
                return p1PubDate > p2PubDate // sort by putting most recent published photo on top.
            }
            return false
        })
        
        self.viewDelegate.photosLoaded(withResult: .success(()))
    }
    
    func showAlert(with message: String) {
        self.coordinatorDelegate.showAlert(with: message)
    }
    
    func showOptionsActionSheet() {
        self.coordinatorDelegate.showOptionsActionSheet()
    }
}
