//
//  PhotoViewModel.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 27/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol PhotoViewModel {
    var photo: Photo { get }
    var title: String { get }
    var publishedDate: String { get }
    var author: String { get }
}

class PhotoViewModelImpl: PhotoViewModel {
    var photo: Photo
    var title: String
    var publishedDate: String
    var author: String
    
    init(photo: Photo) {
        self.photo = photo
        title = photo.title ?? ""
        author = photo.author ?? ""
        publishedDate = (photo.publishedDate != nil) ? photo.publishedDate!.formattedDate() : ""
    }
}
