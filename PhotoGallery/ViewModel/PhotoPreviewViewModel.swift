//
//  PhotoPreviewViewModel.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 28/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol PhotoPreviewViewModelCoordinatorDelegate: class {
    func dismissView()
    func showActivityView(with items:[Any])
}

// MARK:- VIEWMODEL PROTOCOL
protocol PhotoPreviewViewModel {
    func dismissView()
    func showActivityView(with items:[Any])
    func photoImage(completion: @escaping (UIImage) -> Void)
    
    var title: String { get }
    var publishedOn: String { get }
    var author: String { get }
}

class PhotoPreviewViewModelImpl: PhotoPreviewViewModel {
    // MARK: PRIVATE VARIABLES
    private weak var coordinatorDelegate: PhotoPreviewViewModelCoordinatorDelegate!
    
    private let selectedPhoto: PhotoViewModel
    
    // MARK: INITIALIZER
    init(selectedPhoto: Photo, coordinatorDelegate: PhotoPreviewViewModelCoordinatorDelegate) {
        self.selectedPhoto = PhotoViewModelImpl(photo: selectedPhoto)
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS / PROPERTIES
    func dismissView() {
        self.coordinatorDelegate.dismissView()
    }
    
    func showActivityView(with items:[Any]) {
        self.coordinatorDelegate.showActivityView(with: items)
    }
    
    var title : String {
        return selectedPhoto.title
    }
    
    var publishedOn: String {
        return "Published on: \(selectedPhoto.publishedDate)"
    }
    
    var author: String {
        return "Shared by: \(selectedPhoto.author)"
    }
    
    func photoImage(completion: @escaping (UIImage) -> Void) {
        if let imageUrl = self.selectedPhoto.photo.imageUrl {
            ImageStore.getImage(withImagePath: imageUrl, { (result: Result<UIImage>) in
                if case let Result.success(image) = result {
                    completion(image)
                }
            })
        }
    }
}
