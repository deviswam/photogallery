//
//  AppDelegate.swift
//  PhotoGallery
//
//  Created by Waheed Malik on 26/01/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Return if this is a unit test
        if let _ = NSClassFromString("XCTest") {
            return true
        }
        
        window = UIWindow()
        appCoordinator = AppCoordinator(window: window!)
        appCoordinator.start()
        window?.makeKeyAndVisible()
        return true
    }
}

