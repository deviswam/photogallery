IMPORTANT NOTES
===============

1. The project fully implements the following user stories.
    *  `Photo gallery` screen that shows recently publicly uploaded photos on `Flickr network` in collection view.
    * User can search for images by keywords (tag) using the search bar.
    * Images downloaded once are cached for that particular session.
    * User can sort/order photos by published date.
    * Photo preview is shown when an image is tapped in the gallery. The photo preview shows the bigger image with its details/ metadata.
    * From Photo preview screen user can save the image to System's gallery (Photo app).
    * From Photo preview screen user can share the image by email (app must be running on a real device to see `mail` option working).
2. `iOS 10` onwards supported. Tested on `iPhone 6/7/8/X` and their `plus versions` in both orientation.
3. The project has been developed on latest `XCode 9.3` with `Swift 4.1` without any warnings or errors.
4. The app is following `MVVM-C (Model-View-ViewModel-Coordinator)` architecture pattern.
5. `TDD (Test driven development)` approach has been adopted by writting test first with XCTest. Only suitable test cases have been written considering the time limitation and effort involved. Project Test suite has `39 test cases` covering most of the core application components and error handling.
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
7. With more time, following things could be implemented/improved:
    * Aiming to achieve 100% test coverage by writing test cases for `Photo Preview` screen and its related MVVM-C module components. Most of those test cases would be same as `Photo gallery` screen's MVVM-C module, whose test cases are already written.
    * Some test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
8. This project is built on best software engineering practices and code structure is easy to follow. To name a few best practices followed: `SOLID principles, design patterns, protocol oriented programming, loosely coupled architecture and TDD`.

Note: This app has been developed and tested thoroughly and carefully, so nothing should go wrong but Incase such happens please inform recruitement agent so that issue can be addressed.

Many thanks for your time.

